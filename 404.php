<?php
    header("HTTP/1.0 404 Not Found");


    echo'
        <!DOCTYPE html>
        <html>

        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <title>Error 404</title>
            <link rel="stylesheet" type="text/css" href="css/reset.css" />
            <link rel="stylesheet" type="text/css" href="css/style404.css" /> 
        </head>
        <body>
            <div id="notfound">
                <div class="notfound">
                    <div class="notfound-404">
                        <h1>404</h1>
                        <h2>Page not found</h2>
                    </div>
                    <a href="index.php">Homepage</a>
                </div>
            </div>

        </body>

        </html>
';
?>
