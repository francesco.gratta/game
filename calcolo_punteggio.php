<?php

    session_start();
    
    $filename = $_SESSION['datiPartita'];
    $domanda = $_SESSION['domandaAttuale'];

     //controllo se siamo a una domanda nuova
    if ($_REQUEST['nuovaDomanda'] == 'si')
    {
        //porto a null la sessione per la domanda successiva
        $_SESSION['domandaAttuale'] = null;
        $_SESSION['visuaAttuale'] = null;
        $_SESSION['rispostaAttuale'] = null;

        header ('location: controllo_vittoria.php?daPage=gioco.php');
    }
    else
    {
        $domanda['risposta'] = 'si';

        if (is_null($_SESSION['punteggioDomande']))
        {
            $_SESSION['punteggioDomande'] = 0;

        }
        if($_REQUEST['risposta'] == $domanda['risposta_corretta'])
        {
             $_SESSION['punteggioDomande']++;
        }
        else
        {
            $_SESSION['punteggioDomande'] = $_SESSION['punteggioDomande'];
        }

        $_SESSION['domandaAttuale'] = $domanda;
        $_SESSION['rispostaAttuale'] = $_REQUEST['risposta'];

        header('location: gioco.php?risposta='.$_REQUEST['risposta']);
    }
    
?>