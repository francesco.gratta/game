<?php
    
    session_start();

    $filename = $_SESSION['datiPartita'];
    $json_data = json_decode(file_get_contents($filename),true);


    if ($_SESSION['giocatore'] == 'giocatore1')
        {
            if(is_null($categoriaG2))
            {
                header('location: controllo_vittoria.php?daPage=calcolo_punti.php');
            }
            if($categoriaG1['punteggio'] > $categoriaG2['punteggio'])
            {
                $_SESSION['punteggioCategorie']++;
                $json_data['puntiG1'] = $_SESSION['punteggioCategorie'];
            }
            if($categoriaG1['punteggio'] == $categoriaG2['punteggio'])
            {
                $_SESSION['punteggioCategorie']++;
                $json_data['puntiG1'] = $_SESSION['punteggioCategorie'];
            }
        }
        else
        {
            if(is_null($categoriaG1))
            {
                header('location: controllo_vittoria.php?daPage=calcolo_punti.php');
            }
            if($categoriaG2['punteggio'] > $categoriaG1['punteggio'])
            {
                $_SESSION['punteggioCategorie']++;
                $json_data['puntiG2'] = $_SESSION['punteggioCategorie'];
            }
            if($categoriaG2['punteggio'] == $categoriaG2['punteggio'])
            {
                $_SESSION['punteggioCategorie']++;
                $json_data['puntiG2'] = $_SESSION['punteggioCategorie'];
            }
        }
        file_put_contents($filename,json_encode($json_data));  
        $_SESSION['changePunti'] = 'si';

        header('location: controllo_vittoria.php?daPage=punteggio.php');

?>