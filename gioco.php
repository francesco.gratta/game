<?php
    session_start();
    
    if(is_null($_SESSION['rispostaAttuale']))
    {
        header('Refresh:4; URL=controllo_vittoria.php?daPage=gioco.php');
    }

    $filename = $_SESSION['datiPartita'];        

    //controllo che l'avversario non abbia abbandonato
    if(is_null($_REQUEST['controllo']!='ok'))
    {
       header('location: controllo_vittoria.php?daPage=gioco.php');
    }


    if(!is_null($_SESSION ['domandaAttuale']) and is_null($_SESSION['rispostaAttuale']))
    {
        $domanda = $_SESSION ['domandaAttuale'];
        if($domanda['risposta'] == 'si')
        {
            header('location: gioco.php?risposta='.$_SESSION['rispostaAttuale']);
        }
    }
        
    //scheltro pagine (uguale per tutti)
    echo '
        <!DOCTYPE html>
            <html>
                <head>
                    <meta charset="utf-8">
                    <title>Trivia</title>
                    <link rel="stylesheet" type="text/css" href="css/reset.css" />
                    <link rel="stylesheet" type="text/css" href="css/style.css" /> 
                    <link rel="shortcut icon" type="image/x-icon" href="img/index.png">
                </head>
                <body>
                <div id="bottom">
                                <p id="user"><img id="user" src="img/user.png">  '.$_SESSION['username'].'  </p>
                                <a href="logout.php" id="bottom">Logout</a>
                            </div>
                <div id="bloccoDomanda">
                    ';

     if($_SESSION['sceltaCategoria'] == 'si')
    {
        $json_data = json_decode(file_get_contents($filename),true);         
        $json_data ['categoria']  = json_decode(file_get_contents('data/'.$_REQUEST['categoria'].'.json'),true);
        $_SESSION['categoria'] = $json_data ['categoria'];
        file_put_contents($filename, json_encode($json_data));    
        $_SESSION['sceltaCategoria'] = 'no';
        
    }
    
   
    //dichiarazione variabile json_data dal file
    $json_data = json_decode(file_get_contents($filename),true);

    //segno categoria come usata
    if(is_null($_SESSION['categorie']))
    {
        $json_data2 = $json_data ['categoria'];
        $categorie['indice'] = 1;
        $categoria['nome_categoria'] = $json_data2['nome_categoria'];
        $categorie[1] = $categoria;
        $_SESSION['categorie'] = $categorie;
    }
    else
    {
        $json_data2 = $json_data ['categoria'];
        $categorie = $_SESSION['categorie'];
        $j = $categorie['indice'];
        $categoria = $categorie[$j];
        if($categoria['nome_categoria'] != $json_data2['nome_categoria'])
        {
            $categorie['indice']++;
            $categoria['nome_categoria'] = $json_data2['nome_categoria'];
            $j = $categorie['indice'];
            $categorie[$j] = $categoria;
            $_SESSION['categorie'] = $categorie; 
        }
    }
    
    //set a zero numero domande iniziali
    if(is_null($_SESSION ['domandaNumero']))
     {
        $_SESSION ['domandaNumero'] = 0;
     }


    $nome_categoria = $json_data['categoria'];
    $nome_categoria = $nome_categoria['nome_categoria'];

    $nome_categoria_sessione = $_SESSION['categoria'];
    $nome_categoria_sessione = $nome_categoria_sessione['nome_categoria'];
    //set categoria nella sessione
    if(is_null($_SESSION['categoria']) or $nome_categoria != $nome_categoria_sessione)
    {
        $_SESSION['categoria'] = $json_data ['categoria'];
    }

    

    if(is_null($_SESSION['rispostaAttuale']))
    {   
        if(is_null($_SESSION['domandaAttuale']))
        {
                 $json_data = json_decode(file_get_contents($filename),true);

                 if($_SESSION ['domandaNumero'] < 5)
                 {   
                     //incremento domandaNumeroG1
                    $_SESSION ['domandaNumero']++;
                    file_put_contents($filename,json_encode($json_data));

                    //parte variabili e configurazioni dati
                    $i = rand(0,10);
                    $domande = $_SESSION["categoria"];
                    $domanda = $domande[$i];
                    $domanda = $domanda[$i];
                    while($domanda['scelta'] == 'si' or is_null($domanda))
                    {
                        $i = rand(0,10);
                        $domande = $_SESSION["categoria"];
                        $domanda = $domande[$i];
                        $domanda = $domanda[0];
                    }
                    //imposta la domanda come gia' usata e risposta
                    $domanda ['scelta'] = 'si';
                    $domande [$i] = $domanda;
                    $_SESSION ['categoria'] = $domande;
                    $risposta_sbagliata=$domanda['risposte_sbagliate'];
                    $risposta_sbagliata=$risposta_sbagliata[0];
                    $visua_domanda [1] = $domanda['risposta_corretta'];
                    $visua_domanda [2] = $risposta_sbagliata[1];
                    $visua_domanda [3] = $risposta_sbagliata[2];
                    $visua_domanda [4] = $risposta_sbagliata[3];
                    
                    shuffle($visua_domanda);
                    $_SESSION['domandaAttuale'] = $domanda;
                    $_SESSION['visuaAttuale'] = $visua_domanda;
                  }
                
            }
            $domanda = $_SESSION['domandaAttuale'];
            $visua_domanda = $_SESSION['visuaAttuale'];
            $risposta_sbagliata = $domanda['risposte_sbagliate'];
            $risposta_sbagliata = $risposta_sbagliata[0];
            $nome_categoria = $json_data['categoria'];
            $nome_categoria = $nome_categoria['nome_categoria'];
            

            //parte mostra a schermo
            echo '<div id="domanda">
                    <p id="domanda">'.$nome_categoria.' - Domanda '.$_SESSION ['domandaNumero'].' / 5</p>
                    <h1 id="domanda">'.$domanda['domanda'].'</h1>
                </div><br>';
            echo '<div id="risposte">
                <ul>';
            echo '<li><a id="risposta" href="calcolo_punteggio.php?risposta='.$visua_domanda [0].'">'.$visua_domanda [0].'</a></li><br>';
            echo '<li><a id="risposta" href="calcolo_punteggio.php?risposta='.$visua_domanda [1].'">'.$visua_domanda [1].'</a></li><br>';
            echo '<li><a id="risposta" href="calcolo_punteggio.php?risposta='.$visua_domanda [2].'">'.$visua_domanda [2].'</a></li><br>';
            echo '<li><a id="risposta" href="calcolo_punteggio.php?risposta='.$visua_domanda [3].'">'.$visua_domanda [3].'</a></li><br>';
            echo '</ul>
            </div>';
    }
    else
    {
        
        
        //parte dichiarazioni variabili   
        $domanda = $_SESSION['domandaAttuale']; 
        $visua_domanda = $_SESSION['visuaAttuale'];
        $risposta_sbagliata = $domanda['risposte_sbagliate'];
        $risposta_sbagliata = $risposta_sbagliata[0];
        $nome_categoria = $json_data['categoria'];
        $nome_categoria = $nome_categoria['nome_categoria'];
        
            

        //parte mostra a schermo
        echo '<div id="domanda">
                <p id="domanda">'.$nome_categoria.' - Domanda '.$_SESSION ['domandaNumero'].' / 5</p>
                <h1 id="domanda">'.$domanda['domanda'].'</h1>
            </div><br>';
        echo '<div id="risposte">
            <ul>';
        
        //mostro visua 0
        if($visua_domanda [0] == $domanda['risposta_corretta'] )
        {
            echo '<li id="li_correzione"><p id="risposta_corretta">'.$visua_domanda [0].'</p></li><br>';
        }
        else if ($_SESSION['rispostaAttuale'] == $visua_domanda [0])
        {
            echo '<li id="li_correzione"><p id="risposta_sbagliata">'.$visua_domanda [0].'</p></li><br>';   
        }
        else
        {
             echo '<li id="li_correzione"><p id="risposta">'.$visua_domanda [0].'</p></li><br>';   
        }
        
        //mostro visua 1
        if($visua_domanda [1] == $domanda['risposta_corretta'] )
        {
            echo '<li id="li_correzione"><p id="risposta_corretta">'.$visua_domanda [1].'</p></li><br>';
        }
        else if ($_SESSION['rispostaAttuale'] == $visua_domanda [1])
        {
            echo '<li id="li_correzione"><p id="risposta_sbagliata">'.$visua_domanda [1].'</p></li><br>';   
        }
        else
        {
             echo '<li id="li_correzione"><p id="risposta">'.$visua_domanda [1].'</p></li><br>';   
        }
        
        //mostro visua 2
        if($visua_domanda [2] == $domanda['risposta_corretta'] )
        {
            echo '<li id="li_correzione"><p id="risposta_corretta">'.$visua_domanda [2].'</p></li><br>';
        }
        else if ($_SESSION['rispostaAttuale'] == $visua_domanda [2])
        {
            echo '<li id="li_correzione"><p id="risposta_sbagliata">'.$visua_domanda [2].'</p></li><br>';   
        }
        else
        {
             echo '<li id="li_correzione"><p id="risposta">'.$visua_domanda [2].'</p></li><br>';   
        }
        
        //mostro visua 3
        if($visua_domanda [3] == $domanda['risposta_corretta'] )
        {
            echo '<li id="li_correzione"><p id="risposta_corretta">'.$visua_domanda [3].'</p></li><br>';
        }
        else if ($_SESSION['rispostaAttuale'] == $visua_domanda [3])
        {
            echo '<li id="li_correzione"><p id="risposta_sbagliata">'.$visua_domanda [3].'</p></li><br>';   
        }
        else
        {
             echo '<li id="li_correzione"><p id="risposta">'.$visua_domanda [3].'</p></li><br>';   
        }
        echo '</ul>
        </div>
        <div id="domanda_successiva">';
        
        //controllo numero domanda per bottone differente
        if($_SESSION['domandaNumero'] < 5)
            {
                echo '<a id="bottom_risposta" href="calcolo_punteggio.php?nuovaDomanda=si">Domanda Successiva</a>';
            }
        else
            {
                echo '<a id="bottom_risposta" href="punteggio.php">Punteggio Parziale</a>';
            }
    }

    echo '</div>
        </body>
        </html>';
     
        
?>