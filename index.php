<?php
        echo '
    <!DOCTYPE html>
    <html>
        <head>
            <meta charset="utf-8">
            <title>Trivia Crack</title>
            <meta name="viewport" content="width=device-width, initial-scale=1">
            
            <link rel="shortcut icon" type="image/x-icon" href="img/index.png">

            
            <link rel="stylesheet" type="text/css" href="css/reset.css" />
            <link rel="stylesheet" type="text/css" href="css/style1.css" />
        </head>
        <body>


            <div class="welcome-section content-hidden">
                <div class="content-wrap">
                    <ul class="fly-in-text">
                        <li>B</li>
                        <li>E</li>
                        <li>N</li>
                        <li>V</li>
                        <li>E</li>
                        <li>N</li>
                        <li>U</li>
                        <li>T</li>
                        <li>O</li>

                    </ul>
                    <p class="tc">IN TRIVIA CRACK</p>
                    <a href="set_user.php" class="enter-button">Enter</a>
                </div>
            </div>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
            <script type="text/javascript">

                $(function() {

                    var welcomeSection = $(".welcome-section"),
                        enterButton = welcomeSection.find(".enter-button");

                   $(".tc").delay(4000).animate({
                    opacity: 1,
                  }, 3000);

                    setTimeout(function() {
                        welcomeSection.removeClass("content-hidden");
                    }, 500);s

                    enterButton.on("click", function(e) {
                        e.preventDefault();
                        welcomeSection.addClass("content-hidden");
                    });


                })();

            </script>

        </body>
    </html>';
?>
