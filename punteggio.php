<?php

    session_start();
    

    header('Refresh:2; URL=controllo_vittoria.php?daPage=punteggio.php');
    
    
    //controllo che l'avversario non abbia abbandonato
    if($_REQUEST['controllo']!='ok')
    {
       header('location: controllo_vittoria.php?daPage=punteggio.php');
    }


    
    //dichiarazione variabile json_data dal file
    $filename = $_SESSION['datiPartita'];
    $json_data = json_decode(file_get_contents($filename),true);
    
    //set punteggio della categoria
    //set categorie in file json e calcolo punteggio categoria
    if($_SESSION['giocatore'] == 'giocatore1')
    {
       
        if ($_SESSION['changePunteggio'] != 'si')
        {
            $categorieG2 = $json_data['categorieG2'];
            $categorieG1 = $_SESSION['categorie'];

            $indice = $categorieG1 ['indice'];
            $categoriaG1 = $categorieG1 [$indice];
            $categoriaG2 = $categorieG2 [$indice];
            $_SESSION['changePunteggio'] = 'si';
            
            if(is_null($_SESSION['punteggioCategorie']))
            {
                $_SESSION['punteggioCategorie'] = 0;
                $json_data['puntiG1'] = $_SESSION['punteggioCategorie'];
            }
            
            $categoriaG1 ['punteggio'] = $_SESSION['punteggioDomande'];
            $categorieG1 [$indice] = $categoriaG1;
            $json_data['categorieG1'] = $categorieG1; 
            $_SESSION['categorie'] = $categorieG1;  
            file_put_contents($filename, json_encode($json_data));
        }
        
    }
    else
    {            
        if($_SESSION['changePunteggio'] != 'si')
        {
            $categorieG1 = $json_data['categorieG1'];
            $categorieG2 = $_SESSION['categorie'];
            
            $indice = $categorieG2 ['indice'];
            $categoriaG2 = $categorieG2 [$indice];
            $categoriaG1 = $categorieG1 [$indice];
            $_SESSION['changePunteggio'] = 'si';
            
            if(is_null($_SESSION['punteggioCategorie']))
            {
                $_SESSION['punteggioCategorie'] = 0;
                $json_data['puntiG2'] = $_SESSION['punteggioCategorie'];
            }
            $categoriaG2 ['punteggio'] = $_SESSION['punteggioDomande'];
            $categorieG2 [$indice] = $categoriaG2;
            $json_data['categorieG2'] = $categorieG2;
            $_SESSION['categorie'] = $categorieG2;  
            file_put_contents($filename, json_encode($json_data));
        }     
        
    }
    
    $categorieG1 = $json_data['categorieG1'];
    $categorieG2 = $json_data['categorieG2'];

    
    echo '
            <!DOCTYPE html>
            <html>
                <head>
                    <meta charset="utf-8">
                    <title>Punteggio</title>
                    <link rel="stylesheet" type="text/css" href="css/reset.css" />
                    <link rel="stylesheet" type="text/css" href="css/style.css" />
                    
                    <link rel="shortcut icon" type="image/x-icon" href="img/punteggio.png">
                </head>
                <body>';
    echo '<div id="bottom"> 
                            <p id="user"><img id="user" src="img/user.png">  '.$_SESSION['username'].'  </p>
                            <a href="logout.php" id="bottom">Logout</a>
                        </div>
                <div id="table">
                <table>
                         <tr>';

    if($json_data['puntiG1'] >= 3 or $json_data['puntiG2'] >= 3)
    {
         
         echo '<td id="title" colspan="3">Punteggio Finale</td>';
        $finale = 'si';
    }
    else
    {    
       echo '<td id="title" colspan="3">Punteggio Parziale</td>';
    }
    echo '</tr>
        <tr>
            <td id="user" colspan="1">'.$json_data['giocatore1'].'</td>
            <td id="categoria1" colspan="1">Categorie</td>
            <td id="user" colspan="1">'.$json_data['giocatore2'].'</td>
        </tr>';



    $categoriaG1 = $categorieG1[1];
    $categoriaG2 = $categorieG2[1];
    //visua Categoria 1
    if(!is_null($categorieG1[1]) and !is_null($categorieG2[1]))
    {   
        $attesa = 'no';
        echo '<tr>
            <td id="punteggio">'.$categoriaG1['punteggio'].'</td>'; 
    if(is_null($categoriaG1))
        {
            echo '<td id="categoria">'.$categoriaG2['nome_categoria'].'</td>';
        }
        else
        {
            echo '<td id="categoria">'.$categoriaG1['nome_categoria'].'</td>';
        }
    echo '<td id="punteggio">'.$categoriaG2['punteggio'].'</td>
        </tr>';
    }
    else if(!is_null($categorieG1[1]) and is_null($categorieG2[1]))
    {
        echo '<tr>
            <td id="punteggio">'.$categoriaG1['punteggio'].'</td>'; 
    if(is_null($categoriaG1))
        {
            echo '<td id="categoria">'.$categoriaG2['nome_categoria'].'</td>';
        }
        else
        {
            echo '<td id="categoria">'.$categoriaG1['nome_categoria'].'</td>';
        }
    echo '<td id="attesa">In attesa</td>
        </tr>';
        $attesa = 'si';
    }
    else if(is_null($categorieG1[1]) and !is_null($categorieG2[1]))
    {
        echo '<tr>
            <td id="attesa">In attesa</td>'; 
    if(is_null($categoriaG1))
        {
            echo '<td id="categoria">'.$categoriaG2['nome_categoria'].'</td>';
        }
        else
        {
            echo '<td id="categoria">'.$categoriaG1['nome_categoria'].'</td>';
        }
    echo '<td id="punteggio">'.$categoriaG2['punteggio'].'</td>
        </tr>';
        $attesa = 'si';
    }
    else
    {
        echo '<tr>
            <td></td>
            <td></td>
            <td></td>
        </tr>';
    }
    

    $categoriaG1 = $categorieG1[2];
    $categoriaG2 = $categorieG2[2];
    //visua Categoria 2
    if(!is_null($categorieG1[2]) and !is_null($categorieG2[2]))
    {   
        $attesa = 'no';
        echo '<tr>
            <td id="punteggio">'.$categoriaG1['punteggio'].'</td>'; 
    if(is_null($categoriaG1))
        {
            echo '<td id="categoria">'.$categoriaG2['nome_categoria'].'</td>';
        }
        else
        {
            echo '<td id="categoria">'.$categoriaG1['nome_categoria'].'</td>';
        }
    echo '<td id="punteggio">'.$categoriaG2['punteggio'].'</td>
        </tr>';
    }
    else if(!is_null($categorieG1[2]) and is_null($categorieG2[2]))
    {
        echo '<tr>
            <td id="punteggio">'.$categoriaG1['punteggio'].'</td>'; 
    if(is_null($categoriaG1))
        {
            echo '<td id="categoria">'.$categoriaG2['nome_categoria'].'</td>';
        }
        else
        {
            echo '<td id="categoria">'.$categoriaG1['nome_categoria'].'</td>';
        }
    echo '<td id="attesa">In attesa</td>
        </tr>';
        $attesa = 'si';
    }
    else if(is_null($categorieG1[2]) and !is_null($categorieG2[2]))
    {
        echo '<tr>
            <td id="attesa">In attesa</td>'; 
    if(is_null($categoriaG1))
        {
            echo '<td id="categoria">'.$categoriaG2['nome_categoria'].'</td>';
        }
        else
        {
            echo '<td id="categoria">'.$categoriaG1['nome_categoria'].'</td>';
        }
    echo '<td id="punteggio">'.$categoriaG2['punteggio'].'</td>
        </tr>';
        $attesa = 'si';
    }
    else
    {
         echo '<tr>
            <td></td>
            <td></td>
            <td></td>
        </tr>';
    }


    $categoriaG1 = $categorieG1[3];
    $categoriaG2 = $categorieG2[3];
//visua Categoria 3
    if(!is_null($categorieG1[3]) and !is_null($categorieG2[3]))
    {
        $attesa = 'no';
        echo '<tr>
            <td id="punteggio">'.$categoriaG1['punteggio'].'</td>'; 
    if(is_null($categoriaG1))
        {
            echo '<td id="categoria">'.$categoriaG2['nome_categoria'].'</td>';
        }
        else
        {
            echo '<td id="categoria">'.$categoriaG1['nome_categoria'].'</td>';
        }
    echo '<td id="punteggio">'.$categoriaG2['punteggio'].'</td>
        </tr>';
    }
    else if(!is_null($categorieG1[3]) and is_null($categorieG2[3]))
    {
        echo '<tr>
            <td id="punteggio">'.$categoriaG1['punteggio'].'</td>'; 
    if(is_null($categoriaG1))
        {
            echo '<td id="categoria">'.$categoriaG2['nome_categoria'].'</td>';
        }
        else
        {
            echo '<td id="categoria">'.$categoriaG1['nome_categoria'].'</td>';
        }
    echo '<td id="attesa">In attesa</td>
        </tr>';
        $attesa = 'si';
    }
    else if(is_null($categorieG1[3]) and !is_null($categorieG2[3]))
    {
        echo '<tr>
            <td id="attesa">In attesa</td>'; 
    if(is_null($categoriaG1))
        {
            echo '<td id="categoria">'.$categoriaG2['nome_categoria'].'</td>';
        }
        else
        {
            echo '<td id="categoria">'.$categoriaG1['nome_categoria'].'</td>';
        }
    echo '<td id="punteggio">'.$categoriaG2['punteggio'].'</td>
        </tr>';
        $attesa = 'si';
    }
    else
    {
         echo '<tr>
            <td></td>
            <td></td>
            <td></td>
        </tr>';
    }

    $categoriaG1 = $categorieG1[4];
    $categoriaG2 = $categorieG2[4];
//visua Categoria 4
    if(!is_null($categorieG1[4]) and !is_null($categorieG2[4]))
    {
        $attesa = 'no';
        echo '<tr>
            <td id="punteggio">'.$categoriaG1['punteggio'].'</td>'; 
    if(is_null($categoriaG1))
        {
            echo '<td id="categoria">'.$categoriaG2['nome_categoria'].'</td>';
        }
        else
        {
            echo '<td id="categoria">'.$categoriaG1['nome_categoria'].'</td>';
        }
    echo '<td id="punteggio">'.$categoriaG2['punteggio'].'</td>
        </tr>';
    }
    else if(!is_null($categorieG1[4]) and is_null($categorieG2[4]))
    {
        echo '<tr>
            <td id="punteggio">'.$categoriaG1['punteggio'].'</td>'; 
    if(is_null($categoriaG1))
        {
            echo '<td id="categoria">'.$categoriaG2['nome_categoria'].'</td>';
        }
        else
        {
            echo '<td id="categoria">'.$categoriaG1['nome_categoria'].'</td>';
        }
    echo '<td id="attesa">In attesa</td>
        </tr>';
        $attesa = 'si';
    }
    else if(is_null($categorieG1[4]) and !is_null($categorieG2[4]))
    {
        echo '<tr>
            <td id="attesa">In attesa</td>'; 
    if(is_null($categoriaG1))
        {
            echo '<td id="categoria">'.$categoriaG2['nome_categoria'].'</td>';
        }
        else
        {
            echo '<td id="categoria">'.$categoriaG1['nome_categoria'].'</td>';
        }
    echo '<td id="punteggio">'.$categoriaG2['punteggio'].'</td>
        </tr>';
        $attesa = 'si';
    }
    else
    {
        echo '<tr>
            <td></td>
            <td></td>
            <td></td>
        </tr>';
    }


    $categoriaG1 = $categorieG1[5];
    $categoriaG2 = $categorieG2[5];
//visua Categoria 5
    if(!is_null($categorieG1[5]) and !is_null($categorieG2[5]))
    {
        $attesa = 'no';
        
        echo '<tr>
            <td id="punteggio">'.$categoriaG1['punteggio'].'</td>'; 
    if(is_null($categoriaG1))
        {
            echo '<td id="categoria">'.$categoriaG2['nome_categoria'].'</td>';
        }
        else
        {
            echo '<td id="categoria">'.$categoriaG1['nome_categoria'].'</td>';
        }
    echo '<td id="punteggio">'.$categoriaG2['punteggio'].'</td>
        </tr>';
    }
    else if(!is_null($categorieG1[5]) and is_null($categorieG2[5]))
    {
        echo '<tr>
            <td id="punteggio">'.$categoriaG1['punteggio'].'</td>'; 
    if(is_null($categoriaG1))
        {
            echo '<td id="categoria">'.$categoriaG2['nome_categoria'].'</td>';
        }
        else
        {
            echo '<td id="categoria">'.$categoriaG1['nome_categoria'].'</td>';
        }
    echo '<td id="attesa">In attesa</td>
        </tr>';
        $attesa = 'si';
    }
    else if(is_null($categorieG1[5]) and !is_null($categorieG2[5]))
    {
        echo '<tr>
            <td id="attesa">In attesa</td>'; 
    if(is_null($categoriaG1))
        {
            echo '<td id="categoria">'.$categoriaG2['nome_categoria'].'</td>';
        }
        else
        {
            echo '<td id="categoria">'.$categoriaG1['nome_categoria'].'</td>';
        }
    echo '<td id="punteggio">'.$categoriaG2['punteggio'].'</td>
        </tr>';
        $attesa = 'si';
    }
    else
    {
         echo '<tr>
            <td></td>
            <td></td>
            <td></td>
        </tr>';
    }
    

    $indice = $categorieG1['indice'];
    $categoriaG1 = $categorieG1[$indice];
    $categoriaG2 = $categorieG2[$indice];

     if ($_SESSION['changePunti'] != 'si' and $attesa == 'no')
        {
            if($_SESSION['giocatore'] == 'giocatore1')
            {
                if($categoriaG1['punteggio'] > $categoriaG2['punteggio'])
                {
                    $_SESSION['punteggioCategorie']++;
                    $json_data['puntiG1'] = $_SESSION['punteggioCategorie'];
                }
                else if($categoriaG1['punteggio'] == $categoriaG2['punteggio'])
                {
                    $_SESSION['punteggioCategorie']++;
                    $json_data['puntiG1'] = $_SESSION['punteggioCategorie'];
                    $json_data['puntiG2']++;
                }
                else
                {
                    $json_data['puntiG2']++;
                }
                
                //set prossimo turno
                if ($json_data['turno'] == 'giocatore1')
                    {
                        $json_data['turno'] = 'giocatore2';
                    }
                    else
                    {
                        $json_data['turno'] = 'giocatore1';
                    }
            }
            else
            {
                if(!is_null($json_data['puntiG1']))
                {
                    $_SESSION['punteggioCategorie'] = $json_data['puntiG2'];    
                }
            }
         
            
         
            file_put_contents($filename, json_encode($json_data));
            $_SESSION['changePunti'] = 'si';
            
    }
    
    if( $_SESSION['changePunti'] != 'si')
    {
        if ($_SESSION['giocatore'] == 'giocatore1')
        {
           echo '
            <tr>
                <td id="punti">'.$json_data['puntiG1'].'</td>
                <td id="punti">PUNTI</td>
                <td id="punti"></td>
        </tr>'; 
        }
        else
        {  
            echo '
            <tr>
                <td id="punti"></td>
                <td id="punti">PUNTI</td>
                <td id="punti">'.$json_data['puntiG2'].'</td>
            </tr>';
        }
        
    }
    else
        {
            echo '
            <tr>
                <td id="punti">'.$json_data['puntiG1'].'</td>
                <td id="punti">PUNTI</td>
                <td id="punti">'.$json_data['puntiG2'].'</td>
            </tr>';
        }
    echo '
    </table>
    </div>
    <div id="pulsante_punti">';

    if($attesa == 'no' and $_SESSION['changePunti'] == 'si')
    {
        if($finale == 'si')
        {
            echo '<a id="punti_caricamento" href="vittoria.php">Esito Finale</a>';
        }
        else if($json_data['turno'] == $_SESSION['giocatore'])
        {
            echo '<a id="punti_caricamento" href="reset_nuova_categoria.php">Scegli Categoria</a>';
        }
        else
        {
            echo '<a id="punti_caricamento" href="reset_nuova_categoria.php">Categoria successiva</a>';
        }
    }
    else if($attesa == 'no')
    {
        echo '<a id="attesa">Caricamento...</a>';
    }
    else 
    {
        if($_SESSION['giocatore'] == 'giocatore1')
        {
            echo '<a id="attesa">Attendi '.$json_data['giocatore2'].'...</a>';
        }
        else
        {
            echo '<a id="attesa">Attendi '.$json_data['giocatore1'].'...</a>';
        }     
    }
    echo'</div>
    </body>
        </html>';
?>