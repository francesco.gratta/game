<?php
    
    session_start();
    
    header('Refresh:8; URL=controllo_vittoria.php?daPage=scegli_categoria.php');

    //controllo che l'avversario non abbia abbandonato
    if(is_null($_REQUEST['controllo']!='ok'))
        header('location: controllo_vittoria.php?daPage=scegli_categoria.php');

    $filename = $_SESSION['datiPartita'];

    $json_data = json_decode(file_get_contents($filename),true);

    
    if($_SESSION['scelto']=='si')
    {
        $_SESSION['giocatore'] = 'giocatore2';
    }
    else
    {
        $_SESSION['giocatore'] = 'giocatore1';
    }  
    

    

    //dichiaro turno iniziale
    if(is_null($json_data['turno']))
    {
        if($json_data['turnoIniziale'] == 'giocatore1')
        {
            $json_data['turno'] = 'giocatore1';
        }
        else
        {
            $json_data['turno'] = 'giocatore2';
        }
        file_put_contents($filename, json_encode($json_data));
    }



    if($_SESSION['giocatore'] == $json_data['turno'])
    {
        $json_data2 = $json_data ['categoria'];
        $nome_categoria = $json_data2 ['nome_categoria'];
        
        $categoria_sessione = $_SESSION['categoria'];
        $nome_categoria_sessione = $categoria_sessione['nome_categoria'];
        
        if(is_null($json_data['categoria']) or $nome_categoria == $nome_categoria_sessione)
        {
            
            
            //sessione scelta Categoria si
            $_SESSION['sceltaCategoria'] ='si';       
            
            //categorie già fatte 
            $categorie = $_SESSION['categorie'];
            
            for($i = 1; $i <= 5; $i++)
            {
                $categoria = $categorie[$i];
                $nome_categoria = $categoria ['nome_categoria'];
                if ($nome_categoria == 'sport')
                {
                    $sport = 'usato';
                }
                if ($nome_categoria == 'geography')
                {
                    $geography = 'usato';
                }
                if ($nome_categoria == 'history')
                {
                    $history = 'usato';
                }
                if ($nome_categoria == 'cinema')
                {
                    $cinema = 'usato';
                }
                if ($nome_categoria == 'science')
                {
                    $science = 'usato';
                }

            }
            
            echo '
                <!DOCTYPE html>
                <html>
                    <head>
                        <meta charset="utf-8">
                        <title>Scelta Categoria</title>
                        <meta name="viewport" content="width=device-width, initial-scale=1">

                        <link rel="shortcut icon" type="image/x-icon" href="img/scelta_categoria.png">


                        <link rel="stylesheet" type="text/css" href="css/reset.css" />
                        <link rel="stylesheet" type="text/css" href="css/style.css" />
                        <link rel="stylesheet" type="text/css" href="css/style2.css" />
                    </head>
                    <body>
                        <div id="bottom_cat">
                                       <p id="user"><img id="user" src="img/user.png">  '.$_SESSION['username'].'  </p>
                                       <a href="logout.php" id="bottom">Logout</a>
                        </div>
                        <div id="scegli_categoria">
                            <h1 id="scegli_categoria">Scegli una categoria disponibile</h1>
                        </div>
                            <nav class="circular-menu">
                            <input class="circular-menu__button" id="circular-menu" type="checkbox" href="javascript: void 0" checked="checked"/>
                            <label class="circular-menu__icon" for="circular-menu">
                                <div class="hamburger hamburger-bar"></div>
                                <div class="hamburger hamburger-bar"></div>
                                <div class="hamburger hamburger-bar"></div>
                            </label>';
                            
                            if($sport != 'usato')
                            {
                                echo '<a class="circular-menu__item" href="gioco.php?categoria=sport"><img src="https://img.icons8.com/metro/26/000000/sport.png"><i class="fa fa-chrome"></i></a>';
                            }
                            else
                            {
                                echo '<a class="circular-menu__item_used"></a>';
                            }
            
                            if($geography != 'usato')
                            {
                                echo '<a class="circular-menu__item" href="gioco.php?categoria=geography"><img src="https://img.icons8.com/metro/26/000000/globe-earth.png"><i class="fa fa-firefox"></i></a>';
                            }
                            else
                            {
                                echo '<a class="circular-menu__item_used"></a>';
                            }
            
                            if($history != 'usato')
                            {
                                echo '<a class="circular-menu__item" href="gioco.php?categoria=history"><img src="https://img.icons8.com/metro/26/000000/cylon-head-new.png"><i class="fa fa-edge"></i></a>';
                            }
                            else
                            {
                                echo '<a class="circular-menu__item_used"></a>';
                            }
            
                            if($cinema != 'usato')
                            {
                                echo '<a class="circular-menu__item" href="gioco.php?categoria=cinema"><img src="https://img.icons8.com/metro/26/000000/movie-projector.png"><i class="fa fa-safari"></i></a>';
                            }
                            else
                            {
                                echo '<a class="circular-menu__item_used"></a>';
                            }
            
                            if($science != 'usato')
                            {
                                echo ' <a class="circular-menu__item" href="gioco.php?categoria=science"><img src="https://img.icons8.com/metro/26/000000/physics.png"><i class="fa fa-opera"></i></a>';
                            }
                            else
                            {
                                echo '<a class="circular-menu__item_used"></a>';
                            }
                            echo'</nav>
                    </body>
                </html>
              ';
        }
        else
        {
            $categoria = $json_data ['categoria'];
            $nome_categoria = $categoria['nome_categoria'];
            header('location: gioco.php?categoria='.$nome_categoria);
        }
        
    }
    else if (!is_null($json_data ['categoria']))
    {
        $json_data2 = $json_data ['categoria'];
        $nome_categoria = $json_data2 ['nome_categoria'];
        
        $categoria_sessione = $_SESSION['categoria'];
        $nome_categoria_sessione = $categoria_sessione['nome_categoria'];
        if($nome_categoria != $nome_categoria_sessione)
        {
            header('location: gioco.php?categoria='.$nome_categoria);  
        }
        else
        {
            header('location: loader.php?daPage=scegli_categoria.php');
        }
        
    }
    else
    {
        header('location: loader.php?daPage=scegli_categoria.php');
    }
    
?>