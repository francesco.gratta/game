<?php

    session_start();
    header("Refresh:5;URL=scegli_categoria.php");

    //controllo che l'avversario non abbia abbandonato
    if($_REQUEST['controllo']!='ok')
        header('controllo_vittoria.php?daPage=scegli_gategoria.php');
    
    $filename = 'data/scelta.json';
    
    if($_SESSION['scelto']!='si' and is_null($_SESSION['turnoIniziale']))
    {
       if(is_null($_SESSION['datiPartita']))
        {   
            for($i=1; $i<=10; $i++)
            {
                if(is_null($filename2))
                {
                    if(file_exists('data/datiPartita'.$i.'.json'))
                    {
                        $json_data2 = json_decode(file_get_contents('data/datiPartita'.$i.'.json'),true);
                        if($json_data2 ['delete'] == 'si')
                        {
                            $filename2 = 'data/datiPartita'.$i.'.json';
                            $_SESSION['datiPartita'] = $filename2;
                            $json_data2 = array();
                            file_put_contents($filename2, json_encode($json_data2));
                            $json_data [$_SESSION['avversario']] = $_SESSION['username'];
                        }     
                    }
                    else
                    {
                        $filename2 = 'data/datiPartita'.$i.'.json';
                        $_SESSION['datiPartita'] = $filename2;
                        $json_data [$_SESSION['avversario']] = $_SESSION['username'];
                    }    
                }
            }       
        }
        echo '
            <!DOCTYPE html>
            <html>
                <head>
                    <meta charset="utf-8">
                    <title>Turno</title>
                    <link rel="stylesheet" type="text/css" href="css/reset.css" />
                    <link rel="stylesheet" type="text/css" href="css/style.css" /> 
                </head>
                <body>
                <div id="bottom">
                            <a href="logout.php" id="bottom">Logout</a>
                        </div>
                    <div id="turno">';
        $filename2 = $_SESSION['datiPartita'];
        $json2_data = json_decode(($filename),true);
        $json2_data [$_SESSION['avversario']] =  $_SESSION['username'];
        file_put_contents($filename, json_encode($json2_data)); 
        
        if(rand(0,1) == 0)
        {
            echo '<h1>Inizi tu </h1><h1 id="user">'.$_SESSION['username'].'</h1><h1> per primo</h1>';
            $json_data['turnoIniziale'] = 'giocatore1';
            $_SESSION['turnoIniziale'] = 'giocatore1';
            $json_data['giocatore1'] = $_SESSION['username'];
            $json_data['giocatore2'] = $_SESSION['avversario'];
            $_SESSION['sceltaCategoria'] = 'si';
            file_put_contents($filename2, json_encode($json_data));
        }     
        else
        {
            echo '<h1>Inizia </h1><h1 id="user">'.$_SESSION['avversario'].'</h1><h1> per primo</h1>';
            $json_data['turnoIniziale'] = 'giocatore2';
            $_SESSION['turnoIniziale'] = 'giocatore2';
            $json_data['giocatore1'] = $_SESSION['username'];
            $json_data['giocatore2'] = $_SESSION['avversario'];
            file_put_contents($filename2, json_encode($json_data));
        }
            
            echo '
                    </div>
                </body>
            </html>';
    }
    else if ($_SESSION['scelto']!='si') 
    {
        echo '
                    <!DOCTYPE html>
                    <html>
                        <head>
                            <meta charset="utf-8">
                            <title>Turno</title>
                            <link rel="stylesheet" type="text/css" href="css/reset.css" />
                            <link rel="stylesheet" type="text/css" href="css/style.css" />
                            <link rel="shortcut icon" type="image/x-icon" href="img/index.png">

                        </head>
                        <body>
                       <div id="bottom">
                                <p id="user"><img id="user" src="img/user.png">  '.$_SESSION['username'].'  </p>
                                <a href="logout.php" id="bottom">Logout</a>
                            </div>
                            <div id="turno">';
                if($_SESSION['turnoIniziale'] == 'giocatore1')
                {
                    echo '<h1>Inizi tu </h1><h1 id="user">'.$_SESSION['username'].'</h1><h1> per primo</h1>';
                    $_SESSION['sceltaCategoria'] = 'si';
                    
                }
                else
                {
                    echo '<h1>Inizia </h1><h1 id="user">'.$_SESSION['avversario'].'</h1><h1> per primo</h1>';
                }
                    echo '
                            </div>
                        </body>
                    </html>';
        
    }
    else
    {
        for($i=1; $i<=10; $i++)
                {
                     if(file_exists('data/datiPartita'.$i.'.json'))
                        {
                            $json_data2 = json_decode(file_get_contents('data/datiPartita'.$i.'.json'),true);
                            if($json_data2 ['giocatore2'] == $_SESSION ['username'])
                            {
                                $filename2 = 'data/datiPartita'.$i.'.json';
                                $_SESSION['datiPartita'] = $filename2;
                                $turnoIniziale = $json_data2['turnoIniziale'];
                                $_SESSION['turnoIniziale'] = $turnoIniziale;
                            }     
                        }
                }          
        echo '
                    <!DOCTYPE html>
                    <html>
                        <head>
                            <meta charset="utf-8">
                            <title>Turno</title>
                            <link rel="stylesheet" type="text/css" href="css/reset.css" />
                            <link rel="stylesheet" type="text/css" href="css/style.css" /> 
                        </head>
                        <body>
                        <div id="bottom">
                                <p id="user"><img id="user" src="img/user.png">  '.$_SESSION['username'].'  </p>
                                <a href="logout.php" id="bottom">Logout</a>
                            </div>
                            <div id="turno">';
                if($turnoIniziale == 'giocatore2')
                {
                    echo '<h1><h1 id="user">'.$_SESSION['avversario'].'</h1><h1> ti ha sfidato!<br>Inizi tu, </h1><h1 id="user">'.$_SESSION['username'].'</h1><h1>, per primo</h1>';
                    $_SESSION['sceltaCategoria'] = 'si';
                }
                else
                {
                    echo '<h1 id="user">'.$_SESSION['avversario'].'</h1><h1> ti ha sfidato!<br>Inizia lui per primo</h1>';
                }
                    echo '
                            </div>
                        </body>
                    </html>';
        
        }
    
?>